# Epidemic_simulation


In the provided nlogo file there is an epidemic simulation (with healthy people, sick people, immunized people, etc). If you open the file on your computer (if NetLogo is installed), you should have on the interface :



- 2 buttons (setup, step)



- 5 monitors (count sains, count contamines, count immunises, count turtles, count_nb_mort)



- one plot



- 6 sliders (nb_sains, nb_contamine, proba_contamines, nb_jours_maladie, proba_deces, and taux_immunise)



On the Code window you should have the code. If the nlogo file doesn't work on your computer you can download the rtf file where the code is, but you will have to copy and paste it on NetLogo and create the buttons, monitors, sliders, and plot yourself.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021

